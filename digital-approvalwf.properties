# -------------------------------------------------------------------------------------------
# NOTE : 
# 	This should be used only for DEV instance
# 	For Live instance, all variables should be defined as Environment variables.
# 	Environment variables hold precedence against what is defined in this file
# -------------------------------------------------------------------------------------------

# Allow local configuration to override Remote Externalized configuration
spring.cloud.config.allowOverride=true
# But, only System properties or Env variables (and not local config files) will override externalized configuration
spring.cloud.config.overrideSystemProperties=false



#############################################################################################
###### Global App Specific Configuration
#############################################################################################
# Logging level (DEBUG,INFO,WARN,FATAL)
logging.level.ROOT=INFO

# Used by Spring Boot (Application Context)
server.contextPath=/igtb-approvalwf

#application name
applicationName=digital-approvalwf-0.4.5

# Port 51001
server.port=51001

#Profile configuration for service
spring.profiles.active=datastore

#Data store name where workflow history / status of request are going to be stored
workflow.dataStore=redis
#Service name from where the user details and entitled users are fetched
workflow.dataFetch=hasuraBO
#Service name from where the day zero workflow details are going to be fetched
workflow.dayZero=elasticSearch

# Module Abbreviation Name (preferably <= 3 characters and only Upper Case Alphabets A-Z)
# e.g. LMs (for Liquidity), IPSH (for Payments), CNR (for Collections & Receivables)...
# This is used for deriving:
# 	- ElasticSearch Index name as <moduleAbbr>-requests (e.g. lms-requests, ipsh-requests, cnr-requests)
#	- Deriving Destination names in Release Trigger and State Update handler modules
ModuleAbbr=HORZ

# Header key and value, to be used as security token while calling APIs for this module
iGTBD-AtomicAPI-SharedKey=I4qwGynNIt5DP5zUjjemHR1mEj8Ii6jq

# ES Database connection details
#ES.DB.Protocol=http
#ES.enrichment.DB.Host=
#ES.enrichment.DB.Port=
#ES.enrichment.DB.User=
#ES.enrichment.DB.Password=

#Redis sentinel details
#Redis.Sentinel.Enabled=false
#Redis.Sentinel.Urls=localhost:6379,localhost:6379
#Redis.DB.Master.Name=redisMaster
#Redis.DB.Instance=0
#Redis.Sentinel.FailoverWaitTimeMS=10000
#Redis.DB.Url=localhost:6379

#CBX
#Quest.Url=http://digital-gatekeeper-dit-int.cfapps.io/quest/graphql

#CBX
#Limit.Url=https://digital-gatekeeper-dit-int.cfapps.io/limits/igtb-limits/v1

#JmsBroker config
#Digital.JmsBroker.Host=192.168.109.146
#Digital.JmsBroker.Port=15672
#Digital.JmsBroker.User=guest
#Digital.JmsBroker.Password=guest
#Digital.JmsBroker.VHost=/

#Digital.RabbitMQ.Host=192.168.109.146
#Digital.RabbitMQ.Port=15672
#Digital.RabbitMQ.User=guest
#Digital.RabbitMQ.Password=guest
#Digital.RabbitMQ.VHost=/

###############################################
# The exchange where all events are published
###############################################
approvalwf.rabbitmq.exchange = cbxevents

########################################
# This property will define the region
#######################################
region=europe

########################################
# This property will define the country
#######################################
country=DNK

##########################################
#Redis key prefix
##########################################
redisKeyPrefix=APPROVALWF:

############################################
#Base 64 audit is required or not
############################################
isBase64AuditRequired=true

##########################################
#Workflow elastic search index name
##########################################
RULES_ES_INDEX_NAME=quest.workflow.rules

##########################################
#Workflow rules elastic search type name
##########################################
RULES_ES_TYPE_NAME=_doc

##########################################
#Product registry elastic search index name
##########################################
PROD_REG_ES_INDEX_NAME=quest.workflow.product_registry_master

##########################################
#Product registry elastic search type name
##########################################
PROD_REG_ES_TYPE_NAME=_doc

########################################################
#Client ID of the digital gatekeeper in eureka registry
########################################################
digital_gatekeeper_eurekaId=digital-gatekeeper

#######################
#Quest url
#######################
quest_url=/quest/graphql

#######################
#configured Limits url
#######################
configured_limits_url=/limits/igtb-limits/v1/configured

#######################
#Limits validation url
#######################
limits_validation_url=/limits/igtb-limits/v1/validate

######################################
#Enabling the feign exception handling
######################################
feign.okhttp.enabled= true

# To Enable/Disable DevMsg field in JSON response returned from API
# Y - enables DevMsg in output json response (may be useful for dev env)
# N - disables the same (recommended for production env) (this is default, if not specified)
EnableDevMsgInResponse=N

###########################################################################################################
# This property will defines the rule order key in which the initiation limits execution order is defined.
###########################################################################################################
workflow_initiation_rules=SelfAuthCheck,RuleAvailabilityCheck,ZeroAuthCheck,PerformInitiation

########################################################################################################
# This property will defines the rule order key in which the approval limits execution order is defined.
########################################################################################################
workflow_approval_rules=ApprovalExpiryCheck,PerformApproval

#####################################################################################
# This property will defines the package in which the rule classes are located and loaded.
#####################################################################################
rules_package=com.igtb.api.action.horz.workflow.rulebook.rules

#####################################################################################
# This property will defines the rule order criteria.
#####################################################################################
rules_order=name

#############################################################################################
# This property will defines the properties file name in which the rule order key is defined.
#############################################################################################
rules_config_filename=application.properties

#############################################
#Criteria for txn logging in workflow log
############################################
txn_log_groupby_criteria=channelSeqId,domainId,userId,service

#############################################
#Txn count redis key expiry in days 
#############################################
TXN_COUNT_REDIS_KEY_EXPIRY_IN_DAYS=1

#############################################
#trashing is applicabled for last action keys  
############################################
last_action_keys=add,amend,initiate,verify,create_template,upload

################################################################
# create,approval of template is applicable for authorize template key  
################################################################
template_txn_prefix=template
create_template_key=create_template
authorize_template_key=authorize_template

isSharedKeyEnabled=true
shared_secret_key= 6c3d57fe-cb79-46d0-945d-c2f8e30120c8

elasticsearch.schemeVersion=7

feign.client.config.default.connectTimeout= 120000
feign.client.config.default.readTimeout= 120000

# Disable Hystrix timeout globally (for all services)
hystrix.command.default.execution.timeout.enabled=false

# Increase the Hystrix timeout to 120s 
hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds=120000

ribbon.ConnectTimeout=120000

ribbon.ReadTimeout=120000

#############################################
#schemeVersion values 5 , 6 and 7, default = 5 
#############################################
elasticsearch.schemeVersion=7

#############################################################################
# service_keys_actions is required while forming the event eg : product/any
#############################################################################
service_keys_actions=upload

#Reference for action initiate.
action_initiation=initiate,add,amend,add_si,amend_si,upload,upload_bulk,create_batch,create_batch_bulk,submit,create


#######################
#Hasura GraphQL url
#######################
#hasura_url=http://10.240.128.105:30378/v1/graphql
hasura_url=http://cbxbo-hasura-fab.apps.openshift.intellectdesign.com/v1/graphql

#############################################
#shared secret key to invoke quest API
#############################################
hasura_admin_secret=cbxadminsecretkey

################################################################################
#The channelId to be respected when fetching data from hasura
################################################################################
hasuraChannel=DIGI

bo_hasura_url=http://cbxbo-hasura-fab.apps.openshift.intellectdesign.com/v1/graphql

hasuraSchemaName=admin

bo_hasuraSchemaName=admin
################################################################################
#The account criteria to be respected when fetching data from hasura
################################################################################
hasuraAccountCriteria=acc,vacc

################################################################################
#FO hasura relation table colomn name from user table
################################################################################
users.user_domain_role_maps=user_domain_role_maps
users.user_domain_role_maps.role=role
users.user_domain_role_maps.domain=domain
users.user_domain_role_maps.customer=customer
users.user_domain_role_maps.role_profiles=role_profiles

# values : domain_guid, domain_name
jwt.domainId.key=domain_guid
# cron expression= second minute hour day month weekday
cron.expression=0 0/59 23 * * *
# cron.expression=*/3 * * * * *
