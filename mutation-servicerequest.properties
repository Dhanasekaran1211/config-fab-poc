# Node environment
NODE_ENV=production
#------------------------------------------------------------------------------
######## E. ENTITLEMENTS CONFIGURATION ######## ::: SECTION START
#
#### E1. Entitlement interface :::: Optional ####
# Possible values: mock | active
# Default: active
# mock: Reads entitlement profile from mock_entl.js in <projectRoot/src> folder
# active: Connects to Entitlement Service
AUTH_MODE=active

#### E2. System user entitlement :::: Required ####
# Possible values: true | false
# Default: false
# true: Indicates that the component is to be considered a system user in entitlements (eg. Gazetteer)
# false: End user entitlements apply
#
SYSTEM_USER_AUTH=true

######## E. ENTITLEMENTS CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## Q. QUERY STORE CONFIGURATION ######## ::: SECTION START
#
#### Q1. Query store parameters used in development/test :::: Optional ####
# Possible values: true | false
# Default: true
# The query store is used as a convenience to load a query/variable set in Graphiql
#
# LOAD_QUERY_STORE=true

#### Q2. Default query set to be loaded :::: Optional ####
# If not provided no query will be loaded
#
DEFAULT_QUERY_LOAD=

######## Q. QUERY STORE CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## A. APPLICATION CONFIGURATION ######## ::: SECTION START
#### IMPORTANT: This must correspond to the schema definition ####
#
# A1. Application name used in Quest GraphQL Schema as Interface name :::: Required ####
APP_NAME_SRQ=ServiceRequests

#### A2. Application code for resolver package ::::  Required ####
# The projection store index name must correspond to this code (with lower casing applied)
APP_CODE_SRQ=SRQ

#### A3. Application Connector for resolver package ::::  Required ####
# The projection store connector option to be used
APP_CONNECTOR_SRQ=elasticSearch

#### A4. Application Connector Protocol for resolver package ::::  Required ####
# The projection store connector protocol option to be used
# NOTE: This will be removed eventually
APP_CONN_PROTOCOL_SRQ=http

### If schema build scope is set as base only then do not pick up
# devDependencies for packs other than the root
# This is typically relevant for execution in standalone mode and not in aggregated mode
SCHEMA_BUILD_SCOPE = baseOnly

######## A. APPLICATION CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## P. PROJECTION STORE CONFIGURATION ######## ::: SECTION START
#
#### P1. CF Service Name for Elastisearch cluster :::: Optional ####
# Default value is elasticsearch
# This is a Quest instance level configuration and will apply to all
# component packakges which will retrieve their configurations
#
# CF_SVC_NAME_ELASTICSEARCH=elasticsearch

#### P2. CF Service Name for specific package :::: Optional ####
# This is only applicable at an individual component package level
# The specific package will bind to the service name specified
# When provided in the Quest configuration serves as an override
#
# Overrides can be provided with a suffix
# ENTL - EntitlementsService
# ACCT - AccountServices
# ORG - OrgService
# FXRATE - FxRatesService
# BUSCAL - BusinessCalendarService
# REFDATA - ReferenceDataService
# LIQI - LiquidityICLService
# LIQS - LiquiditySweepsService
# LIQC - LiquidityCommonService
# PAY - PaymentsService
# CNR - CNRService
#
# Examples
# CF_SVC_NAME_ELASTICSEARCH_ENTL=entl-els-projection
# CF_SVC_NAME_ELASTICSEARCH_ORG=org-els-projection

#### P3. ES Connection string :::: Optional ####
# This is only used in local development.
# Is not considered when deployed in CF
#
# Elasticsearch connection string
# e.g. https://myuser:pwd@esserver.com:9200
ES_CONNSTRING_GLOBAL=http://localhost:9200

#### P4. ES Server Partition for indexes :::: Optional ####
# Default value: N/A (empty)
# Indexes will be created/accessed with this prefix
# e.g. if Partition has a value of "DIT" then the index for Account Services will be "dit.acct"
#
#ES_PARTITION=quest

#### P5. Response size (limit) for Search queries :::: Optional ####
# Default value: 2000
# Search queries will throttle the number of records retrieved based on this value
#
# ES_QUERY_LIMIT=2000

#### P6. Response size (limit) for Aggregate queries :::: Optional ####
# Default value: 50
# Aggregate queries will throttle the number of buckets retrieved based on this value
#
# ES_AGG_QUERY_LIMIT=50

#### P7. Timeout threshold in ms :::: Optional ####
# Default value: 30000 ms
#
# ES_TIMEOUT=30000

######## P. PROJECTION STORE CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## E. REGISTRY CONFIGURATION ######## ::: SECTION START
#
#### E1. Enable discovery :::: Optional ###
# Possible values: true | false
# Default value: true
#
# EUREKA_DISCOVERY_ENABLED=true

#### E2. Eureka discovery name :::: Optional ####
# Default value: digital-quest
# If there are multiple quest contexts supported this must be provided
#
# EUREKA_DISCOVERY_NAME=digital-quest

#### E3. Eureka service name :::: Optional ####
# Default value: digital-registry
# Eureka service name to connect for discovery services
#
# CF_SVC_NAME_DIGITAL_REGISTRY=digital-registry

#### E4. Eureka registration method :::: Optional ####
# Possible values: direct | route
# Default value: route
# Eureka registration supporting
# :: direct: Direct container access registering IP
# :: route: Route is registered
#
# EUREKA_REGISTRATION_METHOD=route

######## E. REGISTRY CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## L. LOG CONFIGURATION ######## ::: SECTION START

#### L1. Logging level :::: Optional ####
# Supported values are
# error - Only errors are logged
# info - Information logs are enabled
# debug - Debug info is included in the logs
# Default value: info
#
LOG_LEVEL=debug

#### L2. Console logging :::: Optional ####
# Possible values: true | false
# Default value: true
# Directed to standard output
#
CONSOLE_LOG=true

#### L3. Logs directed to file :::: Optional ####
# Possible values: true | false
# Default value: false
# Directed to file
# Default location is /logs/quest.log
#
FILE_LOG=false

#### L4. File name for File Logs :::: Optional ####
# Will be available in logs folder
#
# FILE_LOG_NAME=quest.log

#### L5. Mutation Logs :::: Optional ####
# Will be available in {typeName}_feed_logs
#
LOG_MESSAGES=false

######## L. LOG CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## R. RESPONSE PARAMETER CONFIGURATION ######## ::: SECTION START
#
#### R1. Include error stack trace :::: Optional ####
# Possible values: true | false
# Default value: false
# Set to true if you require stack traces to be included in logs/ errors section in response
#
ERROR_STACK_TRACE=false

#### R2. Include error path :::: Optional ####
# Possible values: true | false
# Default value: false
# ERROR_PATH=true

#### R3. Include timer information in meta :::: Optional ####
# NOTE: To be made optional with default value as false
# Possible values: true | false
# Default value: false
# TIMER=false

######## R. RESPONSE PARAMETER CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## S. SECURITY CONFIGURATION ######## ::: SECTION START

#### S1. User claims location :::: Optional ####
# Possible values: bearer | header
# Default value: bearer
# :: bearer: bearer id_token in Authorization header
# :: header: custom igtb_headers - igtb_user/igtb_domain
#
USER_CLAIMS_LOCATION=bearer

#### S2. Validate User token :::: Optional ####
# Possible values: true | false
# Default value: true
# Verify signature and expiry of token
# NOTE: Must be able to toggle both of the above independently
#
USER_TOKEN_VALIDATION=false

#### S3. Verify Shared Secret :::: Required ####
# Possible values: true | false
# Verify shared secret for system inquiries (within the delivery tier)
#
# VERIFY_SHARED_SECRET=true

######## S. SECURITY CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## M. MISCELLANEOUS CONFIGURATION ######## ::: SECTION START

#### M1. Dependant packages :::: Optional ####
# NOTE: This will be phased out
#
DEP_PKGS=true

#### M2. GraphIql enabled :::: Optional ####
# NOTE: This is for development/test usage
#
GRAPHIQL_ENABLED=true

#### M3. Quest route prefix used in GraphIql :::: Optional ####
# Possible value: quest
# Default value: N/A (empty)
# Required to support Graphiql route in Gatekeeper
#
# QUEST_ROUTE_PREFIX=

#### M4. Apollo engine configuration :::: Optional ####
APOLLO_ENGINE=false
APOLLO_ENGINE_KEY=service:prasvenk007-DigitalQuest:SGRBE4lyBnr4yU4R09W-HA
APOLLO_TRACING=false
APOLLO_CACHE_CONTROL=false

#### M5. NewRelic APM Configuration :::: Optional ####
NEW_RELIC_LICENSE_KEY=9a0343bb1dd3aecf0b2da2e92302b503098cec38

#### M6. Development only :::: Conditionally required ####
# NOTE: This will be phased out eventually when JWT or SHARED SECRET will be required
# If token verification is not required then custom headers can be provided
# for user identity transmission in Request
#
DEV_USER=DHartm390
DEV_DOMAIN=premierGroup

#### M7. Codacy project token :::: Optional ####
# Required for development
# Integration with Codacy
#
CODACY_PROJECT_TOKEN=ac5bb3ea175d4c8fad957a727de874f3

#### M8. Greeting on Server Start :::: Optional ####
GREETING=Welcome

#### Mocking enabled :::: Required ####
MOCKS_ENABLED=false

######## M. MISCELLANEOUS CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## K. KAFKA CONFIGURATION ######## ::: SECTION END
#
# If component type is ingestion then additional ingestion related bootstrapping is required

#------------------------------------------------------------------------------
SERVICEREQUEST_NAMESPACE='e043c77b-3b57-48bf-b7f5-f3064a895825'
#------------------------------------------------------------------------------
# # # S3 details
# STORAGE_KEY=S3
# AWS_ACCESS_KEY_ID=AKIA42PVANWCEEADJC3T
# AWS_SECRET_ACCESS_KEY=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
# AWS_BUCKET=bbl-pam
# S3_FILE_PATH=metadata.fileId
# WORKFLOW_INITIATOR_DOC_KEY=https://cbx.igtb.com/workflow-initiator
# #------------------------------------------------------------------------------
# ######## C. CONFIG CONFIGURATION ######## ::: SECTION START
# #### C1. Quest Private Key :::: Required ####
# # Used if Loading of externalised configurations is false
# igtbPrivateKeys.entl = -----BEGIN PRIVATE KEY-----MIIBUwIBADANBgkqhkiG9w0BAQEFAASCAT0wggE5AgEAAkEAwzEq4yDRATlxqxPZITo/Tmhqv3FvsEtfB/n1ia3ZxFBdKPSvWW9A4FhlkmCAOZyuqkSpGXWB4BWpMHhH+a60BwIDAQABAkBLkdSV3NWuUKwmXOfaim+KDrkNZ4CjU3r2Xprmc+10qC9fToTGHnXDPRuoolJfVGtTo9XEJ0AewCnu7PX+oWIpAiEA4gwn8XJHCwc22TjloXqUUOjHrKwDYNxdfqWKG1cjvCsCIQDdDlpOaG5X6cpBVkL5cktX9Cp9kA1xhLNEEXG2JfwNlQIgVpBDJ5IUpKOBnJPOVBVGAOnztSs2K/yXjS1FgwzOsXsCIGez6QeplEJn6jt2lVrilJgBcsGPB89+en7vVBIohbN1AiAeZkMkkvUC8HGLiJUAW7OCpb4ioRX7PqschgHDgWVl5Q==-----END PRIVATE KEY-----
# #### C2. Quest Certificate :::: Required ####
# # Used if Loading of externalised configurations is false
# igtbCertificates.entl = -----BEGIN CERTIFICATE-----MIIBtjCCAWCgAwIBAgIEXC8IITANBgkqhkiG9w0BAQsFADBiMQswCQYDVQQGEwJJTjELMAkGA1UECAwCVE4xDDAKBgNVBAcMA0NITjESMBAGA1UECgwJSW50ZWxsZWN0MRUwEwYDVQQLDAxpZ3RiLWNieC5jb20xDTALBgNVBAMMBGVudGwwHhcNMTkwMTA0MDcxNTQ1WhcNMjAwMTA0MDcxNTQ1WjBiMQswCQYDVQQGEwJJTjELMAkGA1UECAwCVE4xDDAKBgNVBAcMA0NITjESMBAGA1UECgwJSW50ZWxsZWN0MRUwEwYDVQQLDAxpZ3RiLWNieC5jb20xDTALBgNVBAMMBGVudGwwXDANBgkqhkiG9w0BAQEFAANLADBIAkEAwzEq4yDRATlxqxPZITo/Tmhqv3FvsEtfB/n1ia3ZxFBdKPSvWW9A4FhlkmCAOZyuqkSpGXWB4BWpMHhH+a60BwIDAQABMA0GCSqGSIb3DQEBCwUAA0EAGOWSOKRYV5kMGQIfBfkfkFBVJNCw+PoippWPWqyDE4cW4GmtVjnnLBAaCvdyqk0a1I3NOs5RHo8yPaVvK9407w==-----END CERTIFICATE-----
# #### C3. IDP Certificate :::: Required ####
# # Used if Loading of externalised configurations is false
# igtbCertificates.NDFBOTFCQjA3RDMyNDhCM0IyREY3RTJGN0IwMkU4RTk5RDlDODEzNw = -----BEGIN CERTIFICATE-----MIIC/zCCAeegAwIBAgIJOqpL6Sw/5g0pMA0GCSqGSIb3DQEBCwUAMB0xGzAZBgNVBAMTEmlndGItY2J4LmF1dGgwLmNvbTAeFw0xNzEyMDUwNjEwMDlaFw0zMTA4MTQwNjEwMDlaMB0xGzAZBgNVBAMTEmlndGItY2J4LmF1dGgwLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMhNIs8y/ylb25wdtyTub0IaagKUbMiLvOeQrXqf5PN0fTSzDItRHbqlWUDv+U4ajzJhND9zyDBefY3ZCxcvC6gIR4blixDtoebjoLW3GxgIdM5DOWxgRFinbZdkpCUgHdvjlXgsPPHQttiTqqRhPFJjHHKaLepqMsD/TAO0KOBLy/UXncKlp1lD6j9ASM+1nycdvitDW31zaUvPFWTFQjnUF84Y9yqhtOJtOHOiZGE2Vz4wLRbGrNYqTcTMZf+R6TqlqgmqOCx9eblqJCjOMfYo+tU0uyLW+1CIexFOT9IUum1KnCZQvMOOrYWbAqb1HCtUAi8s4toYk2T/P+zfJwsCAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUITR+sINi+t7sX4aYeJhQJRjNjO8wDgYDVR0PAQH/BAQDAgKEMA0GCSqGSIb3DQEBCwUAA4IBAQCO6L0TLFrH2QaUtc0cKrLFPwGhqLmIoyP3cYOjjT0ytASEssWzjPJobGNNC5AuRo96tNeC+t4Keh6YhcBrUbuZXQDa2OCM75xY4D/lY3VyXBDAicMZhHLc/urD+LTyWcIljFN6ZIYEgnwkpADzVDSiju+gCn3mVoDvidl8Bk2xNi411GDh20CeF49bz0mrYxXxwKqVtEx6LK2Mam/+4hKR7bgrrReeF06P1XNwMO/YZsZPE2jori7UdhhvDOWivdE2kbGGuvRh7IzY7hs7lOyE57T4z35O29JLABF0E9k/CER2YjvfrDwNf6ttUUIyU/T3AR4I7LuMSMQ2oVa3OzUu-----END CERTIFICATE-----
# #### C4. KID External Certificate :::: Required ####
# # Used if Loading of externalised configurations is false
# QUEST_PRIVATE_KEY=-----BEGIN PRIVATE KEY-----MIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAtZhKxc8WNYC7W9v/pujVd3f5845CjItDavh9mXAoIsW5OhiAD229v796BkEd/Z4yFg4u4qeLj14XTN4FFID+ZwIDAQABAkAAp/8RLZk8x+0lZ6tKAkpkhvb3cIVWC691L1ZeMRURYcL6kh2mfJX336/Bx61KG0sOQhlhLy96qNEj7TWB8RFlAiEA33mhWvM0JeS0b35uFWeaHKryX+u8oCxljYFnkca0XvMCIQDQBkNhti6nCEZZ1bkEIs7DNlYHlkjImq3PbnxJWFjHvQIgPga2YFYMbVATPLbsuwgsYDSaXUFC83ofHa2DH5T1Tz0CIQCGSSTCNNrU1JCleUPgGaAjgDX029zMMp204/uzVSaqeQIgF4rWschznVKJT8TqZrPgVcL9/xroE8q6Kg+F1CPlCu4=-----END PRIVATE KEY-----
# KEYSTORE_KID_IDP=library-default-v1
# #.............
# igtbPrivateKeys.library-default-v1=-----BEGIN PRIVATE KEY-----MIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAtZhKxc8WNYC7W9v/pujVd3f5845CjItDavh9mXAoIsW5OhiAD229v796BkEd/Z4yFg4u4qeLj14XTN4FFID+ZwIDAQABAkAAp/8RLZk8x+0lZ6tKAkpkhvb3cIVWC691L1ZeMRURYcL6kh2mfJX336/Bx61KG0sOQhlhLy96qNEj7TWB8RFlAiEA33mhWvM0JeS0b35uFWeaHKryX+u8oCxljYFnkca0XvMCIQDQBkNhti6nCEZZ1bkEIs7DNlYHlkjImq3PbnxJWFjHvQIgPga2YFYMbVATPLbsuwgsYDSaXUFC83ofHa2DH5T1Tz0CIQCGSSTCNNrU1JCleUPgGaAjgDX029zMMp204/uzVSaqeQIgF4rWschznVKJT8TqZrPgVcL9/xroE8q6Kg+F1CPlCu4=-----END PRIVATE KEY-----
# igtbCertificates.library-default-v1=-----BEGIN CERTIFICATE-----MIIBDzCBuqADAgECAgRaMZAeMA0GCSqGSIb3DQEBCwUAMA8xDTALBgNVBAoMBGlndGIwHhcNMTcxMjEzMjAzOTU4WhcNMjcxMjEzMjAzOTU4WjAPMQ0wCwYDVQQKDARpZ3RiMFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALWYSsXPFjWAu1vb/6bo1Xd3+fOOQoyLQ2r4fZlwKCLFuToYgA9tvb+/egZBHf2eMhYOLuKni49eF0zeBRSA/mcCAwEAATANBgkqhkiG9w0BAQsFAANBADh+bEQb0V821KIc4svhj6rZnbklrsEdi2jlcfkvUHbKohUILhA3JNXfJ7Ss6FJDznfVbnQ09+SLyZ/3KyVeQU4=-----END CERTIFICATE-----
# WORKFLOW_INITIATOR_EXCHANGE=cbx.wflw.init
# WORKFLOW_INITIATOR_ROUTING_KEY=business.americas.USA.contact/any.base64



MQ_ENABLED=true
RMQ_ALLOW_CONFIG=true
# Exchange
RMQ_EXCHANGE=cbxevents
# DL Exchange
RMQ_DLE=cbxevents-dle
# Queue
RMQ_QUEUE=mutation-servicerequest-state-events
# Routing Key
RMQ_KEY=business.*.*.*.plain
# DLQ
RMQ_DLQ=dlq-mutation-servicerequest-state-events
# DLK
RMQ_DLKEY=mutation-servicerequest-state-events.dlq
# Prefetch
RMQ_PREFETCH=25
# Consumer Tags
RMQ_TAG=quest
RMQ_INTERNAL_TAG=quest-org
# Internal Exchange
RMQ_INTERNAL_EXCHANGE=orgevents
# Queue for internal Exchange
RMQ_INTERNAL_QUEUE=SERVICEREQUEST-FORWARDED-INPUT-QUEUE
# Use err routing logic
RMQ_USE_ERR_ROUTING=true
# Forwarded message TTL ms (time to live)
RMQ_FORWARD_TTL=3600000
# Retry delay in ms
RMQ_RETRY_DELAY=10000
APP_INSTANCE_REGION=Americas
EVENT_SERVICE_CTX_FILTER=account/chequebookrequest,account/cash
CF_SVC_NAME_RABBITMQ=rabbitmq
SVC_URI_RABBITMQ=amqp://fab:s3cr3t@10.197.12.8:5672
QUEST_PACKAGE_TYPE=ingestion
INGESTION_MODEL=ServiceRequest
APP_ALTCONNECTOR_SRQ=redis
APP_ALTCONN_PROTOCOL_SRQ=http
REDIS_ENABLED=true
SVC_HOST_REDIS=10.197.12.11
SVC_PWD_REDIS=s3cr3t
SVC_PORT_REDIS=6379
LOG_LEVEL=debug
ES_LOG_LEVEL=trace
ES_VERSION_SCHEME=7

# Notification RabbitMQ config
# Exchange
RMQ_NOTIFICATION_EXCHANGE=cbxevents
# Prefetch
RMQ_NOTIFICATION_PREFETCH=25
# RMQ Subscription Queue
RMQ_NOTIFICATION_QUEUE=notifications-queue
# RMQ Subscription Key
RMQ_NOTIFICATION_KEY=alert.notification


KAFKA_ENABLED=true
# INGESTION_MODEL=CashAccount
# Kafka service URI
SVC_URI_KAFKA=10.197.12.15:9092
# Consumer Group Id
KAFKA_CONSUMER_GROUPID=servicerequest-feed-group
# Kafka Topic for consumption
KAFKA_SUBSCRIPTION_TOPIC=SERVICEREQUEST-BACKEND-EVENT
# Kafka Retry Topic
KAFKA_RETRY_TOPIC=SERVICEREQUEST-BACKEND-RETRY-TOPIC
# Kafka processing retries
KAFKA_MAX_RETRIES=1
# Kafka DLQ Topic - For quarantining messages that failed processing
KAFKA_DLQ_TOPIC=DLQ-SERVICEREQUEST-BACKEND-INPUT-TOPIC
# Connection time out
KAFKA_CONN_TIMEOUT=30000
# Heartbeat Interval
KAFKA_HEARTBEAT_INTERVAL=3000
# Number of conn retries
KAFKA_CONN_RETRIES=5
KAFKA_MESG_CONTEXT=servicerequest-backend-ingestion

